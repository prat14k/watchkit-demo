//
//  AllLocationController.swift
//  WatchKitLocationSaveApp Extension
//
//  Created by Prateek Sharma on 07/01/18.
//  Copyright © 2018 Prateek Sharma. All rights reserved.
//

import WatchKit
import Foundation
import WatchkitDemoProxyBundle
import CoreData

class AllLocationController: WKInterfaceController {

    @IBOutlet var locationTableView: WKInterfaceTable!
    var moc : NSManagedObjectContext!
    var locationArray = [Location]()
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        moc = CoreDataHelper.managedObjectContext()
        
        locationArray = CoreDataHelper.fetchEntities(className: NSStringFromClass(Location.self), predicate: nil, sortDescriptor: nil, managedObjectContext: moc) as! [Location]
        locationTableView.setNumberOfRows(locationArray.count, withRowType: "LocationRow")
        
        for i in 0..<locationTableView.numberOfRows {
            let rowView = locationTableView.rowController(at: i) as! LocationRowController
            rowView.titleLabel.setText(locationArray[i].locationName)
        }
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
