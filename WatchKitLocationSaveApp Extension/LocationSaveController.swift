//
//  LocationSaveController.swift
//  WatchKitLocationSaveApp Extension
//
//  Created by Prateek Sharma on 07/01/18.
//  Copyright © 2018 Prateek Sharma. All rights reserved.
//

import UIKit
import WatchKit

class LocationSaveController: WKInterfaceController {

    @IBOutlet var mapView: WKInterfaceMap!
    
    @IBAction func doneAction() {
        pop()
    }
    
}
