//
//  Location+CoreDataProperties.swift
//  Watchkit App
//
//  Created by Prateek Sharma on 28/12/17.
//  Copyright © 2017 Prateek Sharma. All rights reserved.
//
//

import Foundation
import CoreData


extension Location {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Location> {
        return NSFetchRequest<Location>(entityName: "Location")
    }

    @NSManaged public var coordinates: NSData?
    @NSManaged public var locationName: String?

}
