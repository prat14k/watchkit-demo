//
//  ViewController.swift
//  Watchkit App
//
//  Created by Prateek Sharma on 28/12/17.
//  Copyright © 2017 Prateek Sharma. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import CoreData
import WatchkitDemoProxyBundle

class ViewController: UIViewController , CLLocationManagerDelegate , UITableViewDataSource, UITableViewDelegate{

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView: MKMapView!
    
    var locationManager : CLLocationManager!
    
    var moc : NSManagedObjectContext!
    var locationsArray = [Location]()
    var currentLocation : CLLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        locationManager = CLLocationManager()
        locationManager.delegate = self
        
        let code = CLLocationManager.authorizationStatus()
        if code == .notDetermined {
            if Bundle.main.object(forInfoDictionaryKey: "NSLocationAlwaysUsageDescription") != nil {
                locationManager.requestAlwaysAuthorization()
            }
            else if code == .restricted || code == .denied {
                print("Cannot be loaded")
            }
        }
        else{
            locationManager.startUpdatingLocation()
        }
        
        moc = CoreDataHelper.managedObjectContext()
        
        loadLocationsData()
    }
    
    func loadLocationsData() {
        
        locationsArray = CoreDataHelper.fetchEntities(className: NSStringFromClass(Location.self), predicate: nil, sortDescriptor: nil, managedObjectContext: moc) as! [Location]
        
        tableView.reloadData()
    }
    
    @IBAction func addLocationAction(_ sender: UIBarButtonItem) {
        
        let alertControl = UIAlertController(title: "Add New Location", message: "Enter the location name to add your current location", preferredStyle: .alert)
        
        alertControl.addTextField(configurationHandler: nil)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let addAction = UIAlertAction(title: "Add Location", style: .default) { (action) in
            
            if alertControl.textFields!.count > 0  &&  self.currentLocation != nil {
                
                if let title = alertControl.textFields?.first?.text , title != "" {
                    
                    let locationManagedObject = CoreDataHelper.insertManagedObject(className: NSStringFromClass(Location.self), managedObjectContext: self.moc) as! Location
                    
                    locationManagedObject.locationName = title
                    
                    let location = CLLocation(latitude: self.currentLocation.coordinate.latitude, longitude: self.currentLocation.coordinate.longitude)
                    locationManagedObject.coordinates = NSKeyedArchiver.archivedData(withRootObject: location) as NSData
                    
                    CoreDataHelper.saveManagedObjectContext(managedObjectContext: self.moc)
                    
                    self.loadLocationsData()
                }
                
            }
            
        }
        
        alertControl.addAction(cancelAction)
        alertControl.addAction(addAction)
        
        self.present(alertControl, animated: true, completion: nil)
        
    }
    
    @IBAction func refreshLocationAction(_ sender: UIBarButtonItem) {
        locationManager.startUpdatingLocation()
        displayLocationInMap()
    }
    
    func displayLocationInMap(){
        if currentLocation != nil {
            mapView.setRegion(MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude), span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)), animated: true)
        }
    }
    
    func displayLocationInMap(_ location : CLLocation){
        
        mapView.setRegion(MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude), span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)), animated: true)
        
        let locationPin = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = locationPin
        
        mapView.addAnnotation(annotation)
        mapView.showAnnotations([annotation], animated: true)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if locations.count > 0 {
            
            let location = locations.first
            
            if currentLocation != nil {
                
                if currentLocation.distance(from: location!) > 500 {
                    currentLocation = location
                    displayLocationInMap()
                }
                
            }
            else {
                currentLocation = location
                displayLocationInMap()
            }
            
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status != .denied && status != .restricted {
            displayLocationInMap()
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locationsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let location = locationsArray[indexPath.row]
        
        cell.textLabel?.text = location.locationName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let locationObject = locationsArray[indexPath.row]
        
        let coordinates = NSKeyedUnarchiver.unarchiveObject(with: locationObject.coordinates! as Data) as! CLLocation
        
        displayLocationInMap(coordinates)
    }
}

