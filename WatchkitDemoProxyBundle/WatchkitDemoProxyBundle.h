//
//  WatchkitDemoProxyBundle.h
//  WatchkitDemoProxyBundle
//
//  Created by Prateek Sharma on 07/01/18.
//  Copyright © 2018 Prateek Sharma. All rights reserved.
//

#import <WatchKit/WatchKit.h>

//! Project version number for WatchkitDemoProxyBundle.
FOUNDATION_EXPORT double WatchkitDemoProxyBundleVersionNumber;

//! Project version string for WatchkitDemoProxyBundle.
FOUNDATION_EXPORT const unsigned char WatchkitDemoProxyBundleVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WatchkitDemoProxyBundle/PublicHeader.h>


